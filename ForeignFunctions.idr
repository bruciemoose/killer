module ForeignFunctions

%include C "sys/types.h"
%include C "signal.h"

export
kill : (pid : Int) -> (signal : Nat) -> IO Int
kill pid signal
  = foreign FFI_C "kill" (Int -> Int -> IO Int) pid (cast signal)
