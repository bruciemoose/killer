.POSIX:
.SUFFIXES:

CC     = idris
CFLAGS = --idrispath .

all: killer
killer:
	$(CC) $(CFLAGS) --output killer Main.idr
clean:
	rm -f *.ibc killer
