module Main

import ForeignFunctions

%default total

Pid : Type
Pid = Int

Signal : Type
Signal = Nat

data Command
  = Add Pid
  | Kill
  | Update
  | Exit
  | UnknownCommand String Nat

data State
  = Running (List Pid)
  | Finished

strToCommand : String -> List String -> Command
strToCommand "add" [pid]
  = Add (cast pid)
strToCommand "kill" []
  = Kill
strToCommand "update" []
  = Update
strToCommand "exit" []
  = Exit
strToCommand cmd args
  = UnknownCommand cmd (length args)

pidsWithZeroExitCodes : (codes : List Int) -> (pids : List Pid) -> List Pid
pidsWithZeroExitCodes codes pids
  = map snd $ filter firstZero (zip codes pids) where
        firstZero : (Int, Int) -> Bool
        firstZero (0, _) = True
        firstZero (_, _) = False

sendTerm : Pid -> IO Int
sendTerm pid = kill pid 15

check : Pid -> IO Int
check pid = kill pid 0

execute : Command -> State -> IO State
execute Update (Running pids)
  = do codes <- for pids check
       pure $ Running (pidsWithZeroExitCodes codes pids)
execute (Add pid) (Running pids)
  = do let pids' = (pid :: pids)
       putStrLn $ "Added process " ++ show pid
       pure $ Running pids'
execute Kill (Running pids)
  = do codes <- for pids sendTerm
       let successes = pidsWithZeroExitCodes codes pids
       putStrLn $ "Successfully sent TERM to: " ++ show successes
       pure $ Running pids
execute (UnknownCommand cmd argCount) (Running pids)
  = do putStrLn $ "Unknown command `" ++ cmd ++ "` with " ++ show argCount ++ " args"
       pure $ Running pids
execute Exit _
  = pure Finished
execute _ Finished
  = pure Finished

handleInput : List String -> State -> IO State
handleInput (cmdstr :: args) state@(Running _)
  = do state' <- execute (strToCommand cmdstr args) state
       case state' of
         (Running pids) =>
           do putStrLn $ "monitored pids: " ++ show pids
              pure state'
         Finished =>
           pure Finished
handleInput _ Finished
  = pure Finished
handleInput _ state
  = pure state

partial
ioRepl : String -> State -> (List String -> State -> IO State) -> IO ()
ioRepl prompt state f = do
  putStr prompt
  line <- getLine
  result <- handleInput (words line) state
  case result of
       state'@(Running _) => ioRepl prompt state' f
       Finished => putStrLn "exiting"

partial
main : IO ()
main = do
  ioRepl "Killer> " (Running [])  handleInput
